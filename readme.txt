Health monitor project
~~~~~~~~~~~~~~~~~~~~~~

how to run
----------

1. go to cpp_training/smartBand
2. open terminal and run "make" command
3. execute "./main"
4. real time data will be displayed in terminal
5. persistant json data is updated in "cpp_training/smartBand/health_monitor.json"


Diagrams
--------
Class diagram and usecase diagram is stored in "cpp_training/Diagrams"



