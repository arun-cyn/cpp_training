#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <random>
#include <fstream>
#include <cstring>
#include "json.h"
#include <ctime>

using namespace std;
using namespace chrono;
using this_thread::sleep_for;

std::mutex m_lock;
std::string m_file_path;

class Log
{
private:
    std::string json_file_path;
public:
    Log(const std::string& file_path);
    template <typename T>
    void write(const char* log, T i);
    string getTimeStamp();
};


Log::Log(const std::string& file_path)
{
    json_file_path = file_path;
}

 string Log::getTimeStamp(){
    auto start = std::chrono::system_clock::now();
    // Some computation here
    auto end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    return (std::ctime(&end_time));
 }

template <typename T>
void Log::write(const char* log, T i)
 {
    std::lock_guard<std::mutex> lock(m_lock);

    std::fstream m_file;
    try{
        m_file.open(json_file_path, std::ios::in);
        if(m_file.good() != 1){
            throw string("Exception Throw: File path does not exist! \n");
        }
    }
    catch(string ex_str){
        std::cout << ex_str;
        exit(0);
    }

    //Json::Reader reader;
    Json::Value json_obj;

    m_file >> json_obj;
    json_obj["health Status"]["time"] = getTimeStamp();
    json_obj["health Status"][log] = i;//msg;
    m_file.close();

    std::cout << json_obj.toStyledString() << std::endl;

    // write updated json object to file
    m_file.open(json_file_path, std::ios::out);
    
    
    m_file << json_obj.toStyledString() << std::endl;
    m_file.close();

 }


class health_monitor : public Log
{
    private:
    float randomTempGenerator(){
        return generateRandomNumber(20.0, 60.0);
    }

    float randomSpo2Generator(){
        return generateRandomNumber(75.0, 100.0);
    }

    int randomHeartRateGenerator(){
        return generateRandomNumber(50, 120);
    }

    int randomBloodPressureGenerator(){
        return generateRandomNumber(80, 150);
    }

    template <class T>
    T generateRandomNumber(T startRange, T endRange)
    {
        return startRange + (T)rand()/((T)RAND_MAX/(T)(endRange-startRange));
    }

    public:
    health_monitor(const string& path = "health_monitor.json"):Log(path)
    {

    }

    void temperature_function()
        {
        float temperature;
         while(1){
                temperature = randomTempGenerator();
                write("Temperature", temperature);
                sleep_for(seconds(1));
            }
        }

    void spo2_function()
        {
            float spo2;
            while(1){
                spo2 = randomSpo2Generator();
                write("SP02", spo2);
                sleep_for(seconds(1));
                }
        }

        void heart_rate_function()
        {
            int heart_rate;
            while(1){
                heart_rate = randomHeartRateGenerator();
                write("Heart Rate", heart_rate);
                sleep_for(seconds(1));
                }
        }

        void blood_pressure_function()
        {
            int bp;
            while(1){
                bp = randomBloodPressureGenerator();
                write("Blood Pressure", bp);
                sleep_for(seconds(1));
                }
        }
};


int main()
{
    health_monitor band1;
    //health_monitor band1;
    std::thread temp_thread(&health_monitor::temperature_function,band1);
    std::thread spo2_thread(&health_monitor::spo2_function,band1);
    std::thread heart_rate_thread(&health_monitor::heart_rate_function,band1);
    std::thread bp_thread(&health_monitor::blood_pressure_function,band1);

    temp_thread.join();   // main thread waits for the thread t to finish
    spo2_thread.join();   // main thread waits for the thread t to finish
    heart_rate_thread.join();   // main thread waits for the thread t to finish
    bp_thread.join();   // main thread waits for the thread t to finish
    

    return 0;
}

